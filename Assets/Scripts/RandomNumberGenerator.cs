﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RandomNumberGenerator : MonoBehaviour
   
{
    public Text TextBox;
    public int[] Number = new int[6];
    public int start = 1;
    public int end = 10;


    public void RandomNumberGenerate()
    {
 
        for (int i =0; i < Number.Length; i++)
        {
            
            Number[i] = Random.Range(start, end);
            TextBox.text = Number.ToString();

        }

    }
}

