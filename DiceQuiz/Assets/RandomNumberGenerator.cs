﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RandomNumberGenerator : MonoBehaviour
{
    public GameObject TextBox;
    public int n1;
    public int n2;
    public int n3;
    public int n4;
    public int n5;

    public void RandomNumberGenerate()
    {
        n1 = UnityEngine.Random.Range(2, 13);
        n2 = UnityEngine.Random.Range(2, 13);
        n3 = UnityEngine.Random.Range(2, 13);
        n4 = UnityEngine.Random.Range(2, 13);
        n5 = UnityEngine.Random.Range(2, 13);
        TextBox.GetComponent<Text>().text = "" + n1 + ", " + n2 + ", " + n3 + ", " + n4 + ", " + n5;
    }
    
    public static double Mean(int[] a, int n)
    {
        int sum = 0;
        for (int i = 0; i < n; i++)
            sum += a[i];

        return (double)sum / (double)n;
    }

    public static double Median(int[] a, int n)
    {
        Array.Sort(a);
        if (n % 2 != 0)
            return (double)a[n / 2];
        return (double)(a[(n - 1) / 2] + a[n / 2]) / 2.0;
    }

    public static void Mode(int[] a, int n)
    {
        int max = a.Max();
        int t = max + 1; 
        int[] count = new int[t];
        for (int i = 0; i < t; i++)
            count[i] = 0;

        for (int i = 0; i < n; i++)
            count[a[i]]++;
        int mode = 0;
        int k = count[0];
        for (int i = 1; i < t; i++)
        {
            if (count[i] > k)
            {
                k = count[i];
                mode = i;
            }
        }
    }

    private static int[] NewMethod()
    {
        return;// { n1, n2, n3, n4, n5};
    }

    public static void Main()
    {
        int[] a = NewMethod();
        int n = a.Length;
        Mean(a, n);
        Median(a, n);
        Mode(a, n);
    }

    
}
    